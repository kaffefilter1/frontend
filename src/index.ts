import {execute, makePromise } from 'apollo-link';
import { WebSocketLink } from 'apollo-link-ws';
import { createHttpLink } from 'apollo-link-http';
import { SubscriptionClient } from "subscriptions-transport-ws";
import gql from 'graphql-tag';
import * as L from 'leaflet';

const GRAPHQL_ENDPOINT = 'wss://localhost:5001/graphql';
const client = new SubscriptionClient(GRAPHQL_ENDPOINT, {
  reconnect: true
});
const wslink = new WebSocketLink(client);

const HTTP_ENDPOINT = 'https://localhost:5001/graphql';
const link = createHttpLink({ uri: HTTP_ENDPOINT });

const initter = {
  query: gql`
		{
		  allSpots {
		    nodes {
		      updatesBySpot(orderBy: MEASURED_DESC, first: 1) {
		        nodes {
		          available
		          measured
		        }
		      }
		      flags
		      floor
		      id
		      areaByArea {
		        name
		      }
		      location {
		        x
		        y
		      }
		    }
		  }
		}
	`
};

makePromise(execute(link, initter))
  .then(data => markSpots(data.data))
  .catch(error => console.log(`received error ${error}`))

const spotter = {
  query: gql`
		 subscription SubSpot {
		  listen(topic: "spotUpdated") {
		    relatedNode {
		      ... on Spot {
		        id
		        updatesBySpot(orderBy: MEASURED_DESC, first: 1) {
		          nodes {
		            available
		            measured
		          }
		        }
		      }
		    }
		  }
		}
	`
};

execute(wslink, spotter).subscribe({
next: data => handleSub(data.data.listen.relatedNode),
  error: error => console.log(`received error ${error}`),
  complete: () => console.log(`complete`),
})

console.log(L);
console.log(L.marker);
console.log(L.map);

// initialize Leaflet
var map = L.map('map').setView({lon: 0, lat: 0}, 2);

// add the OpenStreetMap tiles
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>'
}).addTo(map);

// show the scale bar on the lower left corner
L.control.scale().addTo(map);

// show a marker on the map
L.marker({lon: 0, lat: 0}).bindPopup('The center of the world').addTo(map);

// Keep track of spots
export let spots = {}

export const tester = {
	"id": 35,
	"flags": [
		"neat"
	],
	"floor": 0,
	"areaByArea": {
		"name": "Unreal Parking"
	},
	"updatesBySpot": {
		"nodes": [
			{ "available": false, "measured": "2020-03-18T19:24:00"}
		]
	},
	"location": { "x": 2, "y": 2}
}

function isAvailable(spot) {
	if (spot.updatesBySpot.nodes[0].available == true) {
		return true
	}
	return false
}

export function addSpot(spot) {
	var marker;
	var text;
	if (isAvailable(spot)) {
		marker = new L.Marker({lon: spot.location.x, lat: spot.location.y});
		text = `Parking spot ${spot.id} is available.`
	} else {
		marker = new L.Marker({lon: spot.location.x, lat: spot.location.y}, {opacity: 0.5});
		text = `Parking spot ${spot.id} is taken.`
	}
	map.addLayer(marker);
	marker.bindPopup(text);
	spots[spot.id] = marker;
  spots[spot.id].dat = spot;
}

const loggerEndpoint ="http://localhost:3333/append";

var queuedString = "";

function enqueueString(dat) {
  queuedString = queuedString + dat;
  if (queuedString.length > 10000) {
    var toSend = {
      body: queuedString,
      method: "POST"
    };
    fetch(loggerEndpoint, toSend)
      .then(res=>{console.log(res)})
      .catch(error=>console.log(error))
    queuedString = "";
  }
}

function handleSub(minSpot) {
  spots[minSpot.id].dat.updatesBySpot.nodes[0].available = minSpot.updatesBySpot.nodes[0].available;
  var pacstamp = Math.round(Date.parse(minSpot.updatesBySpot.nodes[0].measured) / 1000);
  var jsstamp = Math.round((new Date()).getTime() / 1000);
  var latency = String(jsstamp - pacstamp);
  // exludes 1, includes 0
  var unique = String(jsstamp + Math.random());
  var toWrite = unique + "," + latency + "\n";
  enqueueString(toWrite);
  spots[minSpot.id].dat.updatesBySpot.nodes[0].measured = minSpot.updatesBySpot.nodes[0].measured;
  updateSpot(spots[minSpot.id].dat);
}

function removeSpot(id) {
	// Doesn't remove from our tracker, will fill up mem unless gc is smart
	map.removeLayer(spots[id]);
}

function updateSpot(spot) {
	if (spots[spot.id] == undefined) {
		addSpot(spot);
	} else {
		removeSpot(spot.id);
		addSpot(spot);
	}
}

function markSpots(ss) {
	var aSpot;
	for (aSpot of ss.allSpots.nodes) {
	addSpot(aSpot);
	}
}

